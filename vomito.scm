;;; Vomito
;;; Copyright (C) 2020 Lucas Ransan <lucas@ransan.tk>
;;; Copyright (C) 2020 Albakham <dev@geber.ga>

;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; vomito [SRCS]           file{,s} and/or director{y,ies}
;;     -o | --output       existing directory
;;     -f | --format       mp3 | opus | vorbis
;;     -q | --quality      |_ can't specify both at the same time
;;     -b | --bitrate      |
;;     -i | --include      copy other file aswell (.lrc, .png, ...)
;;     -F | --force        override existing files


;; The main function
(define (main args)
  (write (parse-args (cdr args))))


;; Function that parse the program arguments (without the program name)
;; It returns a list of improper lists:
;; ((output . "./path/to/directory") (format . "opus") (bitrate . "320")
;;  (include . #t) (input . ("/path/to/directory" "path/to/other/directory"))
(define (parse-args args)
  (if (null? args)
      ;; Base case
      '()

      ;; We use let* instead of let because we refer to a variable
      ;; defined earlier in it, which we can't do with let
      (let* (
             ;; First elem in args (which we know isn't an empty list)
             (arg (car args))

             ;; Function that tells if one of the strings in the input list
             ;; matches arg
             (mtch? (lambda (strs)
                      (some? (map (lambda (str)
                                    (string=? str arg))
                                  strs)))))

        (cond
          ((mtch? '("-o" "--output"))
           (cons (cons 'output (cadr args)) (parse-args (cddr args))))
          ((mtch? '("-f" "--force"))
           (cons (cons 'format (cadr args)) (parse-args (cddr args))))
          ((mtch? '("-q" "--quality"))
           (cons (cons 'quality (cadr args)) (parse-args (cddr args))))
          ((mtch? '("-b" "--bitrate"))
           (cons (cons 'bitrate (cadr args)) (parse-args (cddr args))))
          ((mtch? '("-i" "--include"))
           (cons (cons 'include #t) (parse-args (cdr args))))
          ((mtch? '("-F" "--force"))
           (cons (cons 'force #t) (parse-args (cdr args))))
          ;; Default case
          (#t (cons (cons 'input arg) (parse-args (cdr args))))))))


;; Return #f if the list is empty or composed only of one or multiple #f
;; else returns #t
(define (some? lst)
  (cond
    ((null? lst) #f)
    ((car lst) #t)
    (#t (some? (cdr lst)))))
